# Movie Ticket Booking System

## Description
This is a Spring Boot project for a Movie Ticket Booking System. The system allows users to browse movies, view showtimes, and book tickets. It includes features like user authentication, movie search, and booking management.
 The system encompasses a range of features to cater to both movie enthusiasts and administrators managing the platform.

### Key Features

#### 1. **User Authentication and Authorization:**
- Secure user registration and login functionality.
- Role-based access control to ensure appropriate permissions for different user types (e.g., regular users, administrators).

#### 2. **Movie Browsing and Searching:**
- Comprehensive catalog of movies with detailed information such as genre, release date, and cast.
- Powerful search functionality allowing users to find movies based on various criteria.

#### 3. **Showtime Viewing and Ticket Booking:**
- Real-time display of movie showtimes at different theaters.
- Intuitive interface for selecting seats and completing the ticket booking process.

#### 4. **User Profile Management:**
- User profiles for personalized experiences.
- Booking history and preferences to enhance user engagement.

### Prerequisites

Before running the application, ensure that you have the following prerequisites installed:

- Java 11
- Maven
- MySQL (or any other relational database)

### Getting Started

1. **Installation:**
    - Clone the repository: `git clone https://gitlab.com/ajaygs1998/movie-ticket-booking-system.git`
    - Navigate to the project directory: `cd movie-ticket-booking-system`

2. **Configuration:**
    - Create a MySQL database and update the `application.properties` file with the database configuration.
    - Customize other configuration properties if needed.

### Usage

1. **Build the Project:**
    - Execute `mvn clean install` to build the project.

2. **Run the Application:**
    - Execute `java -jar target/movie-ticket-booking-system.jar` to start the application.

### API Documentation

Detailed API documentation is available [here](#).

### Tools/Technologies Used

1. **Java 8+:**
    - The project is developed using Java, providing a robust and scalable foundation.

2. **Maven:**
    - Maven is used for project management and build automation, simplifying the build process.

3. **IntelliJ idea:**
    - IntelliJ's idea is utilized for development, offering a feature-rich integrated development environment.

4. **Apache Tomcat:**
    - Apache Tomcat serves as the application server for deploying and running the Spring Boot application.

5. **Spring Core, Spring Security, Spring Data JPA (Hibernate):**
    - Leveraging the power of the Spring framework for core functionality, security, and data access.

6. **MySQL:**
    - The relational database management system MySQL is employed for data storage and retrieval.

7. **Postman:**
    - Postman is used for testing and validating API endpoints during development.

8. **Swagger - API Documentation:**
    - Swagger is integrated for automatic generation of API documentation, ensuring clarity and ease of use.

### Contributing

If you're interested in contributing to the project, please contact me on [my email](mailto:ajaygs1998@gmail.com).

