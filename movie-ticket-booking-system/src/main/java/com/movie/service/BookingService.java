package com.movie.service;

import java.util.List;

import com.movie.entity.Booking;
import com.movie.request.BookingRequest;
import com.movie.response.BookingResponse;

import jakarta.validation.Valid;

public interface BookingService {

	BookingResponse createBooking(BookingRequest bookingRequest, Long showId, String userId);

	BookingResponse upateBooking(BookingRequest bookingRequest, @Valid Long bookingId);

	List<BookingResponse> getAllBookingsForUser(String emailId);

	Booking getBookingByBookingId(Long bookingId);


	void cancelBooking(Long bookingId);

}
