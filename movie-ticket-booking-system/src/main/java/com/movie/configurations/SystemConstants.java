package com.movie.configurations;

public class SystemConstants {
    public static final int MIN_RATING = 1;
    public static final int MAX_RATING = 10;
    // Private constructor to prevent instantiation of this class
    private SystemConstants() {

    }
}
